# Contributing Guidelines

Thank you for considering contributing to the personal wallpapers repository! Your contributions help make this
collection more diverse and vibrant for everyone to enjoy.

## How to Contribute

To contribute to this repository, please follow these steps:

1. **Fork the Repository:** Start by forking this repository to your own account. This will create a copy of the
   repository under your username.

2. **Clone the Repository:** Clone the forked repository to your local machine using Git.

   ```bash
   git clone https://gitlab.com/YOUR_USERNAME/wallpapers.git
   ```

3. **Add Your Wallpaper:** Add your wallpaper image to the repository. Make sure the image has a resolution of
   1920x1080 pixels.

4. **Commit Changes:** Commit your changes to your local repository.

   ```bash
   git add .
   git commit -m "Add new wallpaper"
   ```

5. **Push Changes:** Push your changes to your forked repository on GitLab.

   ```bash
   git push origin main
   ```

6. **Create a Pull Request:** Finally, create a pull request from your forked repository to the main repository at
   [https://gitlab.com/agdell/wallpapers](https://gitlab.com/agdell/wallpapers). Be sure to provide a clear description
   of your contribution in the pull request.

## Contribution Guidelines

- **Image Format:** Please ensure that your wallpaper image is in JPEG format (`your-wallpaper-name.jpeg`).
- **Image Size:** Ensure that your wallpaper image has a resolution of 1920x1080 pixels.
- **Licensing:** By contributing your wallpaper image to this repository, you agree to license it under the same terms
  as the existing wallpapers. Ensure that you have the necessary rights to distribute the image.
- **Quality:** We strive to maintain a high standard of quality in this repository. Please only contribute wallpapers
  that are visually appealing and suitable for a wide audience.

## Get Help

If you need any assistance or have questions about contributing, feel free to reach out to me via the contact
information provided in my profile.

Thank you for your contribution! 🎉
