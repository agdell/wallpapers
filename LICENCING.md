# Licencing for all Images

## Blobs

[Dark](dark/blobs.jpeg) | [Light](light/blobs.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

## Drool

[Dark](dark/drool.jpeg) | [Light](light/drool.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

## Morphogenesis

[Dark](dark/morphogenesis.jpeg) | [Light](light/morphogenesis.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

## Neogeo

[Dark](dark/neogeo.jpeg) | [Light](light/neogeo.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

## Pills

[Dark](dark/pills.jpeg) | [Light](light/pills.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

## Serenity

[Dark](dark/serenity.jpeg) | [Light](light/serenity.jpeg)

Copyright &copy; 2023-2024 Dika Setya <dikasetyaprayogi@gmail.com>

Licensed under [CC-BY-SA-4.0].

## Sun/Comet

[Dark](dark/sun-comet.jpeg) | [Light](light/sun-comet.jpeg)

Copyright &copy; 2023-2024 axo1otl <bluepancake72@gmail.com>.

Licensed under [CC-BY-SA-4.0].

## Symbolic

[Dark](dark/symbolic.jpeg) | [Light](light/symbolic.jpeg)

Copyright &copy; 2011-2023 Jakub Steiner <jimmac@gmail.com>

Licensed under [CC-BY-SA-3.0].

[CC-BY-SA-3.0]: licences/CC-BY-SA-4.0.txt "Creative Commons Attribution-ShareAlike 3.0 Unported"
[CC-BY-SA-4.0]: licences/CC-BY-SA-4.0.txt "Creative Commons Attribution-ShareAlike 4.0 International"
